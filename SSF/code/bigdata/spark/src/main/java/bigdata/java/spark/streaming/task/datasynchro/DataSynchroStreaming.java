package bigdata.java.spark.streaming.task.datasynchro;

import bigdata.java.framework.spark.kafka.KafkaOffsetPersist;
import bigdata.java.framework.spark.kafka.KafkaTools;
import bigdata.java.framework.spark.kafka.SKProcessor;
import bigdata.java.framework.spark.kafka.SimpleSparkKafka;
import bigdata.java.framework.spark.util.JsonUtil;
import bigdata.java.framework.spark.util.MySqlUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author
 */
public class DataSynchroStreaming {
    public static void main(String[] args) throws InterruptedException {
        //appname:FindDataStreaming topics:yxgk.a_Cashchk_Flow groupid:FindDataStreaming duration:2 maxnum:2000 checkpoint:true
        SimpleSparkKafka simpleSparkKafka = new SimpleSparkKafka(args,5);
        simpleSparkKafka.execute(new SKProcessor() {
            @Override
            public void load(JavaStreamingContext jsc, KafkaOffsetPersist kafkaOffsetPersist) {
                kafkaOffsetPersist.clearPersist();
            }

            @Override
            public void process(JavaInputDStream<ConsumerRecord<String, String>> directStream) {

                JavaDStream<DataModel> mapDStream = map(directStream);

                JavaDStream<DataModel> filter = filter(mapDStream);

                foreachRDDSink(filter);

                KafkaTools.printKafkaOffset(directStream);

            }
        }).start();
    }
    public static void foreachRDDSink(JavaDStream<DataModel> javaDStream)
    {
        javaDStream.foreachRDD(new VoidFunction<JavaRDD<DataModel>>() {
            @Override
            public void call(JavaRDD<DataModel> javaRDD) throws Exception {
                javaRDD.foreachPartition(new VoidFunction<Iterator<DataModel>>() {
                    @Override
                    public void call(Iterator<DataModel> iterator) throws Exception {

                        while (iterator.hasNext())
                        {
                            DataModel next = iterator.next();

                            Tuple3<String, String, String> CASHCHK_ID= next.getPrimaryValue("CASHCHK_ID");
                            if(CASHCHK_ID!=null)
                            {
                                if(CASHCHK_ID._2().equals("2019000055914675"))
                                {
                                    System.out.println(next.getJson());
                                }
                            }

                        }
                    }
                });
            }
        });
    }

    public static JavaDStream<DataModel> filter(JavaDStream<DataModel> javaDStream)
    {
        JavaDStream<DataModel> filter = javaDStream.filter(new Function<DataModel, Boolean>() {
            @Override
            public Boolean call(DataModel v1) throws Exception {
                return v1 != null;
            }
        });
        return filter;
    }

    public static JavaDStream<DataModel> map(JavaInputDStream<ConsumerRecord<String, String>> directStream)
    {
        JavaDStream<DataModel> map = directStream.map(new Function<ConsumerRecord<String, String>, DataModel>() {
            @Override
            public DataModel call(ConsumerRecord<String, String> v1) throws Exception {
                String line = v1.value();
                DataModel dataModel = new DataModel();
                try {
                    JSONObject jsonObject = JSON.parseObject(line);
                    String table = jsonObject.getString(JsonUtil.TABLE);
                    //表名
                    if (StringUtils.isEmpty(table)) {//没有表名
                        return null;
                    } else {
                        dataModel.setTable(table);
                    }

                    //主键
                    JSONArray array = jsonObject.getJSONArray(JsonUtil.PRIMARY_KEYS);
                    if (array == null || array.size() < 1) {//没有主键
                        return null;
                    } else {
                        List<String> keys = JSONArray.parseArray(array.toJSONString(), String.class);
                        List<Tuple3<String,String,String>> KeysList = new ArrayList<>();
                        for (int i = 0; i < keys.size(); i++) {
                            String key = keys.get(i);
                            Tuple3<String,String,String> tuple3 = new Tuple3<>(key,"","");
                            KeysList.add(tuple3);
                        }
                        dataModel.setPrimary_Keys(KeysList);
                    }

                    //操作类型
                    String op_type = jsonObject.getString(JsonUtil.OP_TYPE);
                    if (StringUtils.isEmpty(op_type)) {//没有操作类型
                        return null;
                    } else {
                        dataModel.setOp_Type(op_type);
                    }
                    JSONObject fieldsJSOIN = null;
                    //字段
                    if(op_type.equals(JsonUtil.INSERT))
                    {
                        fieldsJSOIN = (JSONObject) jsonObject.get(JsonUtil.AFTER);
                    }
                    else if(op_type.equals(JsonUtil.UPDATE))
                    {
                        fieldsJSOIN = (JSONObject) jsonObject.get(JsonUtil.AFTER);
                    }
                    else if(op_type.equals(JsonUtil.DELETE))
                    {
                        fieldsJSOIN = (JSONObject) jsonObject.get(JsonUtil.BEFORE);
                    }
                    else
                    {
                        return null;
                    }

                    //字段类型推断
                    List<Tuple3<String,String,String>> fieldsList = new ArrayList<>();
                    List<Tuple3<String, String, String>> primary_keys_new = new ArrayList<>();
                    for(Map.Entry<String,Object> entry : fieldsJSOIN.entrySet() )
                    {
                        String key = entry.getKey();
                        Object value = entry.getValue();
                        if(value==null)
                        {
                            continue;
                        }
                        String type = "";
                        String typeName = value.getClass().getTypeName();
                        Tuple3<String,String,String> tuple3=null;
                        type = DataModel.covertType(typeName);

                        boolean isKey = false;
                        List<Tuple3<String, String, String>> primary_keys = dataModel.getPrimary_Keys();
                        for (int i = 0; i < primary_keys.size(); i++) {
                            Tuple3<String, String, String> keyStr = primary_keys.get(i);
                            if(keyStr._1().equals(key))
                            {//如果是主键，那么设置类型，和值，并且不会放入到fields
                                primary_keys_new.add(new Tuple3<>(key,value.toString(),type));
                                isKey=true;
                                continue;
                            }
                        }
                        if(isKey)
                        {//如果是主键，则不放入fields;
                            continue;
                        }

                        tuple3 = new Tuple3<>(key,value.toString(),type);
                        fieldsList.add(tuple3);
                    }
                    dataModel.setPrimary_Keys(primary_keys_new);
                    dataModel.setFields(fieldsList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dataModel.setJson(line);
                //构建sql语句
                dataModel.createSql();
                return dataModel;
            }
        });
        return map;
    }
}
