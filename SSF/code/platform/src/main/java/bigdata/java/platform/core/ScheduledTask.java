package bigdata.java.platform.core;

import bigdata.java.platform.beans.SysSet;
import bigdata.java.platform.beans.TTask;
import bigdata.java.platform.beans.TaskLog;
import bigdata.java.platform.beans.livy.StartResoult;
import bigdata.java.platform.service.TaskLogService;
import bigdata.java.platform.service.TaskService;
import bigdata.java.platform.service.UserService;
import bigdata.java.platform.util.Comm;
import bigdata.java.platform.util.LivyUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang3.StringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;


import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@DisallowConcurrentExecution
public class ScheduledTask extends QuartzJobBean {
    static final String FORMAT_YYYY_MM_DDHHMMSS="yyyy-MM-dd HH:mm:ss";
    static final String FORMAT_YYYY_MM_DD="yyyy-MM-dd";
    static final TimeZone timeZone = TimeZone.getTimeZone("Asia/Shanghai");
    static final FastDateFormat DateFormatYMDHMS = FastDateFormat.getInstance(FORMAT_YYYY_MM_DDHHMMSS,timeZone);
    static final FastDateFormat DateFormatYMD = FastDateFormat.getInstance(FORMAT_YYYY_MM_DD,timeZone);

    @Autowired
    TaskService taskService;
    @Autowired
    TaskLogService taskLogService;
    @Autowired
    SysSet sysSet;
    @Autowired
    UserService userService;
    static final LoggerBuilder loggerBuilder = new LoggerBuilder();
    public static final ch.qos.logback.classic.Logger taskErrLog = loggerBuilder.getLogger("taskerr");
    public static final ch.qos.logback.classic.Logger taskInfoLog = loggerBuilder.getLogger("taskinfo");
    static final ExecutorService threadPool = Executors.newFixedThreadPool(6);
    public static final Map<Integer,Integer> JiYa =Collections.synchronizedMap(new HashMap<Integer,Integer>());
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        synchronized (ScheduleLock.class)
        {
            List<TTask> tTasks = taskService.list();
            final CountDownLatch countDownLatch = new CountDownLatch(tTasks.size());
            Map<String,String> header = new HashMap<>();
            header.put("X-Requested-By","user");
            for (int i = 0; i < tTasks.size(); i++) {
                TTask tTask = tTasks.get(i);
                threadPool.execute(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            exec(tTask,header);
                        }
                        catch (Exception e)
                        {
                            try{
                                taskService.setStatus(tTask.getTaskId(),TTask.STOPED,new Date());
                                taskErrLog.error("taskScheduler execute fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+e.getMessage()+"\nStackTarce: "+Comm.getStackTrace(e));
                            }
                            catch (Exception ex)
                            {
//                                ex.printStackTrace();
                                taskErrLog.error("taskScheduler execute fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+ex.getMessage()+"\nStackTarce: "+Comm.getStackTrace(ex));
                            }
                        }
                        finally {
                            countDownLatch.countDown();
                        }
                    }
                });
            }
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            taskInfoLog.info("taskScheduler执行完毕!");
        }
    }

    //10停止,11停止中,20启动,21启动中,22Livy提交中
    public void exec(TTask tTask ,Map<String,String> header )
    {
        if(tTask.getStatus().intValue() == TTask.STARTING)
        {
            start(tTask,header);
        }
        else if(tTask.getStatus().intValue() == TTask.LIVYSTARTING)
        {
            updateAppId(tTask,header);
        }
        else if(tTask.getStatus().intValue() == TTask.STOPING)
        {
            //start------2019年9月2日优雅关闭修改(删除代码)
//            stop(tTask,header);
            //end------2019年9月2日优雅关闭修改(删除代码)
        }
        else if(tTask.getStatus().intValue() == TTask.STARTED)
        {
            monitor(tTask,header);
        }
    }

    public void monitor(TTask tTask,Map<String,String> header)
    {
        TaskLog taskLog=null;
        String resoult="";
        Map<String,Object> map=null;
        String applicationId=null;
        try
        {
            taskLog =tTask.getTaskLog();
            applicationId = taskLog.getApplicationid();
            String url = taskLog.getSparkui()+"metrics/json";
            resoult = Comm.httpGet(url);
            map = JSONObject.parseObject(resoult, Map.class);
            Map gauges = (Map)map.get("gauges");
            String waitingBatchesKey = applicationId+".driver." + tTask.getAppName()+".StreamingMetrics.streaming.waitingBatches";
            String totalCompletedBatchesKey = applicationId+".driver." + tTask.getAppName()+".StreamingMetrics.streaming.totalCompletedBatches";
            String totalProcessedRecordsKey = applicationId+".driver." + tTask.getAppName()+".StreamingMetrics.streaming.totalProcessedRecords";
            Integer waitingBatches = 0;
            Integer totalCompletedBatches= 0;
            Long totalProcessedRecords =0L;
            if(gauges.get(waitingBatchesKey) !=null
                    && gauges.get(totalCompletedBatchesKey)!=null
                    && gauges.get(totalProcessedRecordsKey)!=null)
            {
                waitingBatches = (Integer)((Map) gauges.get(waitingBatchesKey)).get("value");
                totalCompletedBatches= (Integer)((Map) gauges.get(totalCompletedBatchesKey)).get("value");
                totalProcessedRecords = Long.valueOf(((Map)gauges.get(totalProcessedRecordsKey)).get("value").toString());
            }

            Integer taskId = null;
            if(tTask!=null)
            {
                taskId = tTask.getTaskId();
            }
            Integer taskLogId = null;
            Integer batchId = null;
            if(taskLog!=null)
            {
                taskLogId = taskLog.getTaskLogId();
                batchId = taskLog.getBatchId();
            }
            taskLogService.updateMonitor(taskLogId,waitingBatches,totalCompletedBatches,totalProcessedRecords);

//            if(tTask.getTaskType().intValue() == 1)
//            {
                Integer activeBatch = Integer.parseInt(tTask.getActiveBatch());
                if(waitingBatches>=activeBatch)
                {
                    try{
                        Integer cishu = JiYa.get(tTask.getTaskId());
                        if(cishu ==null)
                        {
                            JiYa.put(tTask.getTaskId(),0);
                            cishu = 0;
                        }
                        if(cishu <=2)
                        {
                            cishu++;
                            JiYa.put(tTask.getTaskId(),cishu);
                            if(cishu == 3) {//只有连续积压次数为3，才发送告警短信
                                String msg = "实时指标【" + tTask.getTaskName() + "】数据积压" + waitingBatches + " batch" + "，" + DateFormatYMDHMS.format(new Date());
    //                            System.out.println(msg);
                                Comm.sendWarnMsg(tTask.getWarnPhone(),msg );
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        taskErrLog.error("sendWarnMsg1 fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+ex.getMessage()+"\nStackTarce: "+Comm.getStackTrace(ex));
                    }
                }
                else
                {
                    Integer cishu = JiYa.get(tTask.getTaskId());
                    if(cishu !=null)
                    {
                        JiYa.put(tTask.getTaskId(),0);
                        if(cishu ==3)
                        {
                            String msg ="实时指标【"+tTask.getTaskName()+"】数据积压解除"+"，"+ DateFormatYMDHMS.format(new Date());
    //                        System.out.println(msg);
                            Comm.sendWarnMsg(tTask.getWarnPhone(),msg);
                        }
                    }
                }

//            }
            taskInfoLog.info("livy monitor success taskName="+tTask.getTaskName()+" taskId="+taskId + " taskLogId="+taskLogId + " batchId=" + batchId );
        }
        catch (Exception e)
        {
            Integer errCount = tTask.getErrCount();
            if(errCount==null)
            {
                errCount = 0;
            }
            if(errCount <3) {
                errCount++;
                tTask.setErrCount(errCount);
                taskService.updateErrCount(tTask.getTaskId(),errCount);
                System.out.println("updateErrCount="+errCount);
            }
            else
            {
                Integer tryCount = tTask.getTryCount();
                if(tryCount==null)
                {
                    tryCount = 0;
                }

                if(tryCount <3)
                {
                    tryCount++;
                    tTask.setTryCount(tryCount);
                    taskService.setStatusAndClearErrCountAndSetTryCount(tTask.getTaskId(),TTask.STARTING,tryCount,new Date());
                }
                else
                {//监控3次发生错误，并且重启程序3次错误，那么就停止程序
                    taskService.setStatusAndClearErrCountAndClearTryCount(tTask.getTaskId(),TTask.STOPED,new Date());
                }

                try{
                    Comm.sendWarnMsg(tTask.getWarnPhone(),"实时指标【"+tTask.getTaskName()+"】发生第"+tryCount+"次错误下线，"+ DateFormatYMDHMS.format(new Date()));
                }
                catch (Exception ex)
                {
                    taskErrLog.error("sendWarnMsg2 fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+ex.getMessage()+"\nStackTarce: "+Comm.getStackTrace(ex));
                }
            }
            taskErrLog.error("livy monitor fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+e.getMessage()+"\nStackTarce: "+Comm.getStackTrace(e));
        }
    }

    public void stop(TTask tTask,Map<String,String> header)
    {
        TaskLog taskLog=null;
        String resoult="";
        try
        {
            taskLog =tTask.getTaskLog();
            resoult = Comm.httpDelete(sysSet.getLivy() + "/batches/" + taskLog.getBatchId(), header);
            if(resoult.equals("{\"msg\":\"deleted\"}"))
            {
                taskLogService.stop(tTask.getTaskId(),taskLog.getTaskLogId(),taskLog.getBatchId());
                Integer taskId = null;
                if(tTask!=null)
                {
                    taskId = tTask.getTaskId();
                }
                Integer taskLogId = null;
                Integer id = null;
                if(taskLog!=null)
                {
                    taskLogId = taskLog.getTaskLogId();
                    id = taskLog.getBatchId();
                }
                JiYa.remove(tTask.getTaskId());
                taskInfoLog.info("livy stop success taskName="+tTask.getTaskName()+" taskId="+taskId + " taskLogId="+taskLogId + " batchId=" + id );
            }
        }
        catch (Exception e)
        {
            taskErrLog.error("livy stop fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+e.getMessage()+"\nStackTarce: "+Comm.getStackTrace(e));
        }
    }

    public void updateAppId(TTask tTask,Map<String,String> header)
    {
        TaskLog taskLog=null;
        StartResoult jsonObject = null;
        String resoult="";
        Integer taskId = null;
        Integer taskLogId = null;
        Integer batchId = null;
        try
        {
            taskLog =tTask.getTaskLog();
            resoult = Comm.httpGet(sysSet.getLivy() + "/batches/" + taskLog.getBatchId(), header);
            jsonObject = JSONObject.parseObject(resoult, StartResoult.class);
            if(tTask!=null)
            {
                taskId = tTask.getTaskId();
            }
            if(taskLog!=null)
            {
                taskLogId = taskLog.getTaskLogId();
            }
            if(jsonObject!=null)
            {
                batchId = taskLog.getBatchId();
            }
            if(jsonObject.getState().equals("running"))
            {
                taskLogService.updateAppId(taskId,taskLogId,batchId,jsonObject);
                try{
                    Comm.sendWarnMsg(tTask.getWarnPhone(),"实时指标【"+tTask.getTaskName()+"】启动成功，"+ DateFormatYMDHMS.format(new Date()));
                }
                catch (Exception ex)
                {
                    taskErrLog.error("sendWarnMsg3 fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+ex.getMessage()+"\nStackTarce: "+Comm.getStackTrace(ex));
                }
                JiYa.put(tTask.getTaskId(),0);
                taskInfoLog.info("livy start success taskName="+tTask.getTaskName()+" taskId="+taskId + " taskLogId="+taskLogId + " batchId=" + batchId );
            }//dead
            else if(jsonObject.getState().equals("dead"))
            {
                taskService.setStatus(tTask.getTaskId(),TTask.STOPED,new Date());
                taskInfoLog.info("livy start dead taskName="+tTask.getTaskName()+" taskId="+taskId + " taskLogId="+taskLogId + " batchId=" + batchId );
            }
            else if(jsonObject.getState().equals("starting"))
            {
                taskInfoLog.info("livy start waiting taskName="+tTask.getTaskName()+" taskId="+taskId + " taskLogId="+taskLogId + " batchId=" + batchId );
            }
        }
        catch (Exception e)
        {
            taskService.setStatus(tTask.getTaskId(),TTask.STOPED,new Date());
            taskErrLog.error("livy start fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+e.getMessage()+"\nStackTarce: "+Comm.getStackTrace(e));
        }
    }

    public void start(TTask tTask,Map<String,String> header)
    {
        String json="";
        String resoult="";
        StartResoult jsonObject = null;
        TaskLog taskLog=null;
        try
        {
            //start--------2019年9月2日优雅关闭修改(新增代码)
            if(!StringUtils.isBlank(tTask.getSaveType()))
            {
                if(tTask.getSaveType().equals("MySql"))
                {
                    taskService.deleteStopbyMark_Mysql(tTask.getAppName());
                }
                else
                {
                    taskService.deleteStopbyMark_Hbase(tTask.getAppName());
                }
            }
            //end--------2019年9月2日优雅关闭修改(新增代码)
            json = LivyUtil.getJson(tTask);

            String curl = "curl -X POST -H \"Content-Type: application/json\" -H \"X-Requested-By:user\" "+sysSet.getLivy()+"/batches --data '" + json +"'";

            taskInfoLog.info("start info taskName="+tTask.getTaskName()+" livy submit script="+curl );

            resoult = Comm.httpPostJson(sysSet.getLivy() + "/batches", header, json);
            jsonObject = JSONObject.parseObject(resoult, StartResoult.class);
            taskLog =tTask.getTaskLog();

            if(taskLog ==null)
            {//如果taskLog为空，那么手动添加一条(可能因为有人在数据库手动删除了对应的taskLog)
                taskLog   = taskLogService.addTaskId(tTask.getTaskId(), new Date());
            }

            if(taskLog.getBatchId()==null)
            {//第一次启动
                taskLogService.updateBatchId(tTask.getTaskId(),taskLog.getTaskLogId(),jsonObject.getId());
            }
            else
            {//非第一次启动
                Comm.httpDelete(sysSet.getLivy() + "/batches/" + taskLog.getBatchId(), header);
                taskLogService.addBatchId(tTask.getTaskId(),jsonObject.getId(),new Date());
            }

            Integer taskId = null;
            if(tTask!=null)
            {
                taskId = tTask.getTaskId();
            }
            Integer taskLogId = null;
            if(taskLog!=null)
            {
                taskLogId = taskLog.getTaskLogId();
            }
            Integer id = null;
            if(jsonObject!=null)
            {
                id = jsonObject.getId();
            }
            taskInfoLog.info("start success taskName="+tTask.getTaskName()+" taskId="+taskId + " taskLogId="+taskLogId + " batchId=" + id );
        }
        catch (Exception e)
        {
            taskService.setStatus(tTask.getTaskId(),TTask.STOPED,new Date());
            taskErrLog.error("start fail taskName="+tTask.getTaskName()+" taskId="+tTask.getTaskId()+"\nError: "+e.getMessage()+"\nStackTarce: "+Comm.getStackTrace(e));
        }
    }
}