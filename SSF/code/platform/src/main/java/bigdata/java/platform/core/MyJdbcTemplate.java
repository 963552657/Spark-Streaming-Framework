package bigdata.java.platform.core;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import javax.sql.DataSource;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class MyJdbcTemplate extends JdbcTemplate {
    public MyJdbcTemplate(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public <T> T queryForObject(String sql, RowMapper<T> rowMapper) throws DataAccessException {
        List<T> query = query(sql, rowMapper);
        return requiredSingleResult(query);
    }

    @Override
    public <T> T queryForObject(String sql, RowMapper<T> rowMapper, Object... args) throws DataAccessException {
        List<T> query = query(sql, rowMapper, args);
        return requiredSingleResult(query);
    }

    @Override
    public Map<String, Object> queryForMap(String sql) throws DataAccessException {
        List<Map<String, Object>> query = query(sql, getColumnMapRowMapper());
        return requiredSingleResult(query);
    }

    @Override
    public Map<String, Object> queryForMap(String sql, Object... args) throws DataAccessException {
        List<Map<String, Object>> query = query(sql, getColumnMapRowMapper(), args);
        return requiredSingleResult(query);
    }

    @Override
    public Map<String, Object> queryForMap(String sql, Object[] args, int[] argTypes) throws DataAccessException {
        List<Map<String, Object>> query = query(sql, args, argTypes, getColumnMapRowMapper());
        return requiredSingleResult(query);
    }

    @Override
    public List<Map<String, Object>> queryForList(String sql) throws DataAccessException {
        List<Map<String, Object>> query = query(sql, getColumnMapRowMapper());
        return query;
    }

    @Override
    public List<Map<String, Object>> queryForList(String sql, Object... args) throws DataAccessException {
        List<Map<String, Object>> query = query(sql, getColumnMapRowMapper(), args);
        return query;
    }

    @Override
    public <T> List<T> queryForList(String sql, Class<T> elementType) throws DataAccessException {
        List<T> query = query(sql, new BeanPropertyRowMapper<>(elementType));
        return query;
    }

    public static <T>T requiredSingleResult(Collection<T> results) throws IncorrectResultSizeDataAccessException
    {
        int size = (results != null ? results.size() : 0);
        if(size ==0)
        {
            return null;
        }
        if( results.size() > 1)
        {
            throw new IncorrectResultSizeDataAccessException(1, size);
        }
        return results.iterator().next();
    }
}
