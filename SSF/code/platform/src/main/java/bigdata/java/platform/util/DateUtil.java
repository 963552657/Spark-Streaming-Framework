package bigdata.java.platform.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 日期时间操作工具类
 */
public class DateUtil {
    /**
     * yyyyMM
     */
    public static final String FORMAT_YYYYMM ="yyyyMM";
    /**
     * yyyyMMdd
     */
    public static final String FORMAT_YYYYMMDD ="yyyyMMdd";
    /**
     * yyyy-MM
     */
    public static final String FORMAT_YYYY_MM ="yyyy-MM";
    /**
     * yyyy-MM-dd
     */
    public static final String FORMAT_YYYY_MM_DD ="yyyy-MM-dd";
    /**
     * yyyy-MM-dd HH:mm:ss
     */
    public static final String FORMAT_YYYY_MM_DDHHMMSS="yyyy-MM-dd HH:mm:ss";

    /**
     * 分钟
     */
    public static final Integer MINUTE=1;

    /**
     * 秒
     */
    public static final Integer SECOND=2;

    /**
     * 中国时区
     */
    public static TimeZone timeZone = TimeZone.getTimeZone("Asia/Shanghai");

    /**
     * 获取当前系统年月时间(yyyyMM)
     * @return 201904
     */
    public static String getYearMonth()
    {
        Date date = new Date();
        return getYearMonth(date, FORMAT_YYYYMM);
    }

    /**
     * 加，减 天
     * @param dt Date对象
     * @param addDay +,-天数
     */
    public static Date addDay(Date dt,Integer addDay)
    {
        //创建Calendar 的实例
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTime(dt);
        //当前时间减去一天，即一天前的时间
        calendar.add(Calendar.DAY_OF_MONTH,addDay);
        //获取前一天日期
        Date frontTime = calendar.getTime();
        return frontTime;
    }

    /**日期字符串转换date
     * @param dateString 日期字符串
     * @param format format格式
     */
    public static Date String2Date(String dateString,String format)
    {
        Date date = null;
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            dateFormat.setTimeZone(timeZone);
            date = dateFormat.parse(dateString);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return date;
    }

    /**
     * 加，减 月
     * @param dt Date对象
     * @param addMonth +,-月
     */
    public static Date addMonth(Date dt,Integer addMonth)
    {
        //创建Calendar 的实例
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTime(dt);
        //当前时间减去一月
        calendar.add(Calendar.MONTH,addMonth);
        Date frontTime = calendar.getTime();
        return frontTime;
    }

    /**
     * 加，减 秒
     * @param dt Date对象
     * @param addSecond +,-秒
     */
    public static Date addSecond(Date dt,Integer addSecond)
    {
        //创建Calendar 的实例
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTime(dt);
        //当前时间减去
        calendar.add(Calendar.SECOND,addSecond);
        Date frontTime = calendar.getTime();
        return frontTime;
    }

    /**
     * 加，减 毫秒
     * @param dt Date对象
     * @param addMilliSecond +,-毫秒
     */
    public static Date addMilliSecond(Date dt,Integer addMilliSecond)
    {
        //创建Calendar 的实例
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTime(dt);
        //当前时间减去
        calendar.add(Calendar.MILLISECOND,addMilliSecond);
        Date frontTime = calendar.getTime();
        return frontTime;
    }

    /**
     * 格式化指定时间
     * @param dt Date对象
     * @param formate 格式化表达式
     * @return 格式化后的字符串
     */
    public static String getYearMonth(Date dt,String formate)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(formate);
        sdf.setTimeZone(timeZone);
        String format_Date = sdf.format(dt);
        return format_Date;
    }


    /**
     * 格式化时间
     * @param dt Date对象
     * @param formate 格式化表达式
     * @return 格式化后的字符串
     */
    public static String getFormateTime(Date dt,String formate)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(formate);
        sdf.setTimeZone(timeZone);
        String format_Date = sdf.format(dt);
        return format_Date;
    }
    /**
     * 根据当前系统时间格式化时间
     * @param formate 格式化表达式
     * @return 格式化后的字符串
     */
    public static String getFormateTime(String formate)
    {
        return getFormateTime(new Date(),formate);
    }
    /**
     * 获取第二天凌晨的时间戳
     * @return this Calendar's time value in milliseconds.
     */
    public static Long getUnixTime()
    {
        //创建Calendar 的实例
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        calendar.add(Calendar.DAY_OF_MONTH,1);
        return calendar.getTimeInMillis();
    }

    /**
     * 时间戳转换时间类型(yyyy-MM-dd HH:mm:ss)
     * @param stamp 时间戳
     */
    public static String stampToDate(Long stamp)
    {
        return stampToDate(stamp,FORMAT_YYYY_MM_DDHHMMSS);
    }
    /**
     * 时间戳转换时间类型
     * @param stamp 时间戳
     * @param format 输出的时间格式
     */
    public static String stampToDate(Long stamp,String format)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date date = new Date(stamp);
        String res = simpleDateFormat.format(date);
        return res;
    }

    /**
     * 获取年月日，截取字符串0-10位（ 2019-05-27 00:00:00）
     * @param dt 字符串格式2019-05-27 00:00:00
     * @return 截取后的日期2019-05-27
     */
    public static String getYYYYMMDD(String dt){
        String dtStr = dt.substring(0, 10).replace("-", "");
        return dtStr;
    }

    /**
     * 获取两个日期的时间差（单位：分钟）
     * @param start 开始时间
     * @param end 结束时间
     * @return 差值，两个时间相等返回0，start大于end返回负数，start小于end返回正数
     */
    public static String getTimeDiff(String start,String end){
        return getTimeDiff(start,end,MINUTE);
    }

    /**
     * 获取两个日期的时间差（单位：DateUtil.MINUTE分钟,DateUtil.SECOND秒）
     * @param start 开始时间
     * @param end 结束时间
     *  @param unit （单位：DateUtil.MINUTE分钟,DateUtil.SECOND秒）
     * @return 差值，两个时间相等返回0，start大于end返回负数，start小于end返回正数
     */
    public static String getTimeDiff(String start,String end,Integer unit){
        SimpleDateFormat fdf =
                new SimpleDateFormat(DateUtil.FORMAT_YYYY_MM_DDHHMMSS);
        DecimalFormat df = new DecimalFormat("#");
        if(start.isEmpty()||start == null) {
            return "";
        }else {
            try {
                Long timeDiff = fdf.parse(end).getTime() - fdf.parse(start).getTime();
                String format = "";
                if(unit==MINUTE)
                {//分钟
                    format = df.format(timeDiff.doubleValue() / (1000 * 60));
                }
                else if(unit == SECOND)
                {//秒
                    format = df.format(timeDiff.doubleValue() / 1000);
                }
                System.out.println(format);
                return format;

            }catch (Exception e)
            {
                throw new RuntimeException(e);
            }
        }
    }
}
