package bigdata.java.platform.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;

/**
 * Config配置工具类
 */
public class ConfigUtil {
    static Config config = ConfigFactory.load("application.properties");
    /**
     * 获取application.properties配置对象
     */
    public static Config getConfig()
    {
        return config;
    }
}
