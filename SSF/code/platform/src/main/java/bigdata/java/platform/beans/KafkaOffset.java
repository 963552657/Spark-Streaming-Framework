package bigdata.java.platform.beans;

import kafka.common.TopicAndPartition;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class KafkaOffset {
    Map<TopicAndPartition, Long> earliestOffsetMap = new HashMap<>();
    Map<TopicAndPartition, Long> lastOffsetMap = new HashMap<>();

    public Map<TopicAndPartition, Long> getEarliestOffsetMap() {
        return earliestOffsetMap;
    }

    public void setEarliestOffsetMap(Map<TopicAndPartition, Long> earliestOffsetMap) {
        this.earliestOffsetMap = earliestOffsetMap;
    }

    public Map<TopicAndPartition, Long> getLastOffsetMap() {
        return lastOffsetMap;
    }

    public void setLastOffsetMap(Map<TopicAndPartition, Long> lastOffsetMap) {
        this.lastOffsetMap = lastOffsetMap;
    }

    String topic;
    String partition;
    String currentOffset;
    String minOffset;
    String maxOffset;

    public long getLag()
    {
        long ma = Long.parseLong(maxOffset);
        long mi = Long.parseLong(minOffset);
        long cm = 0;
        if(StringUtils.isBlank(currentOffset))
        {
            cm = mi;
        }
        else
        {
            cm = Long.parseLong(currentOffset);
        }

        if(!StringUtils.isBlank(currentOffset))
        {
            cm = Long.parseLong(currentOffset);
            if(cm < mi)
            {
                cm =mi;
            }
        }

        long lng = ma - cm;
        return lng;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getPartition() {
        return partition;
    }

    public void setPartition(String partition) {
        this.partition = partition;
    }

    public String getCurrentOffset() {
        return currentOffset;
    }

    public void setCurrentOffset(String currentOffset) {
        this.currentOffset = currentOffset;
    }

    public String getMinOffset() {
        return minOffset;
    }

    public void setMinOffset(String minOffset) {
        this.minOffset = minOffset;
    }

    public String getMaxOffset() {
        return maxOffset;
    }

    public void setMaxOffset(String maxOffset) {
        this.maxOffset = maxOffset;
    }
}
